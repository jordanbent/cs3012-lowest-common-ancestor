
## CS3012 - Jordan Bent

TCD CS3012 - Software Engineering

# Lowest Common Ancestor

In graph theory and computer science, the lowest common ancestor (LCA) of two nodes *v* and *w* in a tree or directed acyclic graph (DAG) T is the lowest (i.e. deepest) node that has both v and w as descendants, where we define each node to be a descendant of itself (so if v has a direct connection from w, w is the lowest common ancestor). - Wikipedia

## Toolkit

**Text Editor** - Sublime 
**Language** - Ruby
