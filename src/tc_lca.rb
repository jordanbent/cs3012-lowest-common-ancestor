# File:  tc_lca.rb

require_relative "Node"
require_relative "BST"
require "test/unit"

class TestLCA < Test::Unit::TestCase
 
  def test_LCA
    # STANDARD TEST 
  	# =>     7
    #      /   \
    #    2      8
    #   / \      \ 
    #  1   5     11
    #     / \   /
    #    4   6 9
    #   /
    #  3

      testN = BST.new()
      testN.insert(7)
      testN.insert(2)
      testN.insert(1)
      testN.insert(8)
      testN.insert(11)
      testN.insert(9)
      testN.insert(5)
      testN.insert(6)
      testN.insert(4)
      testN.insert(3)
      # testing if path function works
      assert_equal("3 4 5 2 7 ", testN.findPath(3)) 
      #testing if path function works
      assert_equal("6 5 2 7 ", testN.findPath(6)) 

      #testing LCA - checking key of expected LCA of 
      #the nodes with 3 and 6, which should be 5
      assert_equal(5,(testN.lca(3,6)).key)
      #allowing a node to be a descendent of itself
      assert_equal(3, (testN.lca(3,3)).key)
      assert_equal(5, (testN.lca(5,6)).key)
  end

  def test_Nil
    #TESTING NIL BST

    testN = BST.new()
    assert_equal(nil, testN.findPath(3)) 
    assert_equal(nil, testN.findPath(6))
    assert_equal(nil,(testN.lca(3,6)))

    #TESTING BAD TEST INPUT
    testN = BST.new()
    testN.insert(7)
    testN.insert(2)
    testN.insert(1)
    testN.insert(8)
    testN.insert(11)
    testN.insert(9)
    testN.insert(5)
    testN.insert(6)
    testN.insert(4)
    testN.insert(3)
      # testing if path function works
      assert_equal(nil, testN.findPath("hello")) 
      #testing if path function works
      assert_equal(nil, testN.findPath("world")) 

      #testing LCA - checking key of expected LCA of 
      #the nodes with 3 and 6, which should be 5
      assert_equal(nil,(testN.lca("bad", "input")))

  end

end


