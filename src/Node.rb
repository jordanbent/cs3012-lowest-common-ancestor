
 class Node

    attr_reader :key, :left, :right, :parent

    def initialize( key )
      return nil if !(key.is_a? Numeric)
      @key = key
      @left = nil
      @right = nil
    end

    def insert( new_key )
      return nil if !(new_key.is_a? Numeric)
      if new_key <= @key
        @left.nil? ? @left = Node.new( new_key ) : @left.insert( new_key )
      elsif new_key > @key
        @right.nil? ? @right = Node.new( new_key ) : @right.insert( new_key )
      end
    end

  end