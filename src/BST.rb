# File:  BST.rb
require_relative "Node"

class BST

  attr_reader :root, :path

  def initialize
    @root = nil
  end

  def insert(key)
    return nil if !(key.is_a? Numeric) or key.nil?
    if @root.nil?
      @root = Node.new(key)
    else 
      @root.insert(key)
    end
  end

  def search( key, node=@root )
    return nil if node.nil? or !(key.is_a? Numeric)
    if key < node.key
      search( key, node.left )
    elsif key > node.key
      search( key, node.right )
    else
      return node
    end
  end

  def searchString( key, node=@root, path)
    
    return nil if node.nil? or !(key.is_a? Numeric)
    path += " "+(node.key).to_s
    if key < node.key
      searchString( key, node.left, path)
    elsif key > node.key
      searchString( key, node.right, path)
    else
      return path
    end
     
  end

  def findPath(key)
    return nil if key.nil? or !(key.is_a? Numeric)
    ans = (searchString(key, @root, ""))
    return nil if ans.nil?
    else return ans.reverse
  end

  def findKey(path1, path2)
    return nil if path1.nil? or path2.nil?
    return ((path1 & path2)[0]).to_i
  end

  def lca(key1, key2)
    return nil if key1.nil? or key2.nil? or !(key1.is_a? Numeric) or !(key2.is_a? Numeric)
     path1 = findPath(key1)
     path2 = findPath(key2)
    return nil if path1.nil? or path2.nil?

    return search((findKey((path1.each_line(" ").to_a), (path2.each_line(" ").to_a))),)
  end

end
